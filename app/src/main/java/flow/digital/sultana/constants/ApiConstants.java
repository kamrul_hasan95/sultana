package flow.digital.sultana.constants;


public interface ApiConstants {

    //API BASE url
    String BASE_URL = "";
    String HEADER_NAME_CONTENT = "Content-Type";
    String CONTENT_TYPE_JSON = "application/json";
    String CONTENT_TYPE_MULTIPART = "multipart/form-data";
}
