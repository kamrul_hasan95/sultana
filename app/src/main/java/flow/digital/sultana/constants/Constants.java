package flow.digital.sultana.constants;


public interface Constants {
    String PACKAGE_NAME = "flow.digital.sultana";
    String CHANNEL_ID = "flow_notification_channel_loc";
    int NOTIFICATION_ID = 341;
    long LOC_UPDATE_INTERVAL = 60000;
}
