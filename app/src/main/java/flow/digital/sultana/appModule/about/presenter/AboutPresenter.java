package flow.digital.sultana.appModule.about.presenter;

import android.content.Context;

public class AboutPresenter {
    private Context context;
    private AboutViewInterface viewInterface;

    public AboutPresenter(Context context, AboutViewInterface viewInterface) {
        this.context = context;
        this.viewInterface = viewInterface;
    }
}
