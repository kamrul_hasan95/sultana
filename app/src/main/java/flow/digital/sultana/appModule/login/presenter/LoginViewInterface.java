package flow.digital.sultana.appModule.login.presenter;

import flow.digital.sultana.base.MvpView;

public interface LoginViewInterface extends MvpView {
    void onLoginSuccess();
    void onLoginFailed();
}
