package flow.digital.sultana.appModule.history.presenter;

import android.content.Context;

public class HistoryPresenter {
    private Context context;
    private HistoryViewInterface viewInterface;

    public HistoryPresenter(Context context, HistoryViewInterface viewInterface) {
        this.context = context;
        this.viewInterface = viewInterface;
    }
}
