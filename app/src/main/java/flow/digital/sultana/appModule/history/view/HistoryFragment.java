package flow.digital.sultana.appModule.history.view;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import flow.digital.sultana.R;
import flow.digital.sultana.appModule.history.presenter.HistoryPresenter;
import flow.digital.sultana.appModule.history.presenter.HistoryViewInterface;
import flow.digital.sultana.base.BaseFragment;
import flow.digital.sultana.utils.ProgressBarHandler;

public class HistoryFragment extends BaseFragment implements HistoryViewInterface {
    private static final String ARG_TITLE = "title";
    private String title;

    ProgressBarHandler progressBarHandler;
    HistoryPresenter presenter;

    public HistoryFragment() {
        //empty constructor for fragment
    }

    public static HistoryFragment newInstance(String title) {
        HistoryFragment fragment = new HistoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            title = getArguments().getString(ARG_TITLE);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBarHandler = new ProgressBarHandler(getContext());
        presenter = new HistoryPresenter(getContext(), this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_history;
    }


    @Override
    public void showProgress() {
        progressBarHandler.hideProgress();
    }

    @Override
    public void hideProgress() {
        progressBarHandler.showProgress();
    }

    @Override
    public void onError(Throwable t) {
        hideProgress();
        t.printStackTrace();
    }
}
