package flow.digital.sultana.appModule.profile.presenter;

import android.content.Context;

public class ProfilePresenter {
    ProfileViewInterface profileViewInterface;
    Context context;

    public ProfilePresenter(Context context, ProfileViewInterface profileViewInterface) {
        this.profileViewInterface = profileViewInterface;
        this.context = context;
    }
}
