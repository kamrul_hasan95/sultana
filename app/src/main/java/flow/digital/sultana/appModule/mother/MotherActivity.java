package flow.digital.sultana.appModule.mother;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.google.android.material.navigation.NavigationView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import butterknife.BindView;
import flow.digital.sultana.R;
import flow.digital.sultana.appModule.about.view.AboutFragment;
import flow.digital.sultana.appModule.history.view.HistoryFragment;
import flow.digital.sultana.appModule.home.view.HomeFragment;
import flow.digital.sultana.appModule.profile.view.ProfileFragment;
import flow.digital.sultana.base.BaseActivity;

public class MotherActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.fragmentContainer)
    FrameLayout fragmentContainer;

    private Fragment fragment;
    private boolean showFragmentAgain = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        fragment = new HomeFragment();
        loadFragment(getResources().getString(R.string.nav_item_help));
        navigationView.setCheckedItem(R.id.nav_help);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_mother;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_help) {
            if (fragment instanceof HomeFragment){

            }else {
                fragment = new HomeFragment();
                loadFragment(getResources().getString(R.string.nav_item_help));
            }
        } else if (id == R.id.nav_histroy) {
            if (fragment instanceof HistoryFragment){

            }else {
                fragment = new HistoryFragment();
                loadFragment(getResources().getString(R.string.nav_item_history));
            }
        } else if (id == R.id.nav_about) {
            if (fragment instanceof AboutFragment){

            }else {
                fragment = new AboutFragment();
                loadFragment(getResources().getString(R.string.nav_item_about));
            }
        } else if (id == R.id.nav_profile) {
            if (fragment instanceof ProfileFragment){

            }else {
                fragment = new ProfileFragment();
                loadFragment(getResources().getString(R.string.nav_item_profile));
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void loadFragment(String fragmentTitle) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentContainer, fragment);
        transaction.commit();

        toolbar.setTitle(fragmentTitle);
    }
}
