package flow.digital.sultana.appModule.splash;

import androidx.appcompat.app.AppCompatActivity;
import flow.digital.sultana.R;
import flow.digital.sultana.appModule.mother.MotherActivity;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

import android.content.Intent;
import android.os.Bundle;

import java.util.concurrent.TimeUnit;

public class SplashActivity extends AppCompatActivity {
    private Disposable disposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }

    @Override
    protected void onPause() {
        super.onPause();
        disposable.dispose();
    }

    @Override
    protected void onResume() {
        super.onResume();
        disposable = Completable.timer(2, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                .subscribe(this::goToLoginActivity);
    }

    private void goToLoginActivity(){
        startActivity(new Intent(this, MotherActivity.class));
    }
}
