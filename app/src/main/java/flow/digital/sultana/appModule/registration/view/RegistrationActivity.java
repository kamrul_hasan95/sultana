package flow.digital.sultana.appModule.registration.view;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.google.android.material.textfield.TextInputLayout;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import flow.digital.sultana.R;
import flow.digital.sultana.appModule.registration.presenter.RegistrationPresenter;
import flow.digital.sultana.appModule.registration.presenter.RegistrationViewInterface;
import flow.digital.sultana.base.BaseActivity;
import flow.digital.sultana.utils.ProgressBarHandler;
import timber.log.Timber;

public class RegistrationActivity extends BaseActivity implements RegistrationViewInterface {
    @BindView(R.id.tilFirstName)
    TextInputLayout tilFirstName;
    @BindView(R.id.tilLastName)
    TextInputLayout tilLastName;
    @BindView(R.id.tilAge)
    TextInputLayout tilAge;
    @BindView(R.id.tilOccupation)
    TextInputLayout tilOccupation;
    @BindView(R.id.tilEmergencyContact)
    TextInputLayout tilEmergencyContact;
    @BindView(R.id.tilContactNumber)
    TextInputLayout tilContactNumber;
    @BindView(R.id.tilPermanentAddress)
    TextInputLayout tilPermanentAddress;
    @BindView(R.id.tilPresentAddress)
    TextInputLayout tilPresentAddress;
    @BindView(R.id.tilUsername)
    TextInputLayout tilUsername;
    @BindView(R.id.tilPassword)
    TextInputLayout tilPassword;
    @BindView(R.id.spnEmergencyRelation)
    Spinner spnEmergencyRelation;

    List<String> emergencyRelation;

    ProgressBarHandler progressBarHandler;
    RegistrationPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new RegistrationPresenter(this, this);
        progressBarHandler = new ProgressBarHandler(this);
        initData();

        spnEmergencyRelation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Timber.d(emergencyRelation.get(position));
            }
        });
    }

    private void initData(){
        emergencyRelation = Arrays.asList(getResources().getStringArray(R.array.emergency_relation));
    }

    @Override
    public int getLayout() {
        return R.layout.activity_registration;
    }

    @OnClick(R.id.btnRegister)
    public void registrationButtonClick(){

    }

    @Override
    public void onRegistrationSuccess() {

    }

    @Override
    public void onRegistrationFailed() {

    }


    @Override
    public void showProgress() {
        progressBarHandler.hideProgress();
    }

    @Override
    public void hideProgress() {
        progressBarHandler.showProgress();
    }

    @Override
    public void onError(Throwable t) {
        hideProgress();
        t.printStackTrace();
    }
}
