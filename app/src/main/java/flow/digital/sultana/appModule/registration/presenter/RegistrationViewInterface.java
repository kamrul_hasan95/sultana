package flow.digital.sultana.appModule.registration.presenter;

import flow.digital.sultana.base.MvpView;

public interface RegistrationViewInterface extends MvpView {
    void onRegistrationSuccess();
    void onRegistrationFailed();
}
