package flow.digital.sultana.appModule.home.presenter;

import android.content.Context;

public class HomePresenter {
    private Context context;
    private HomeViewInterface viewInterface;

    public HomePresenter(Context context, HomeViewInterface viewInterface) {
        this.context = context;
        this.viewInterface = viewInterface;
    }
}
