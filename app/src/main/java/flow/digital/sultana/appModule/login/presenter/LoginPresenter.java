package flow.digital.sultana.appModule.login.presenter;

import android.content.Context;

public class LoginPresenter {
    private Context context;
    private LoginViewInterface viewInterface;

    public LoginPresenter(Context context, LoginViewInterface viewInterface) {
        this.context = context;
        this.viewInterface = viewInterface;
    }
}
