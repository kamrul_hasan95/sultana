package flow.digital.sultana.appModule.login.view;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.textfield.TextInputLayout;

import butterknife.BindView;
import butterknife.OnClick;
import flow.digital.sultana.R;
import flow.digital.sultana.appModule.login.presenter.LoginPresenter;
import flow.digital.sultana.appModule.login.presenter.LoginViewInterface;
import flow.digital.sultana.appModule.registration.view.RegistrationActivity;
import flow.digital.sultana.base.BaseActivity;
import flow.digital.sultana.utils.ProgressBarHandler;

public class LoginActivity extends BaseActivity implements LoginViewInterface {
    @BindView(R.id.tilUsername)
    TextInputLayout tilUsername;
    @BindView(R.id.tilPassword)
    TextInputLayout tilPassword;

    ProgressBarHandler progressBarHandler;
    LoginPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new LoginPresenter(this,this);
        progressBarHandler = new ProgressBarHandler(this);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_login;
    }

    @OnClick(R.id.btnLogin)
    public void loginButtonClick(){

    }

    @OnClick(R.id.btnRegister)
    public void registrationButtonClicked(){
        startActivity(new Intent(this, RegistrationActivity.class));
    }

    @Override
    public void onLoginSuccess() {

    }

    @Override
    public void onLoginFailed() {

    }

    @Override
    public void showProgress() {
        progressBarHandler.hideProgress();
    }

    @Override
    public void hideProgress() {
        progressBarHandler.showProgress();
    }

    @Override
    public void onError(Throwable t) {
        hideProgress();
        t.printStackTrace();
    }
}
