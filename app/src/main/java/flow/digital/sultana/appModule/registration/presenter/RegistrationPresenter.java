package flow.digital.sultana.appModule.registration.presenter;

import android.content.Context;

public class RegistrationPresenter {
    private Context context;
    private RegistrationViewInterface viewInterface;

    public RegistrationPresenter(Context context, RegistrationViewInterface viewInterface) {
        this.context = context;
        this.viewInterface = viewInterface;
    }
}
