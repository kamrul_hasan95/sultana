package flow.digital.sultana.base;

public interface MvpView {
    void showProgress();

    void hideProgress();

    void onError(Throwable t);
}
